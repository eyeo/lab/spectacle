# Sensible Privacy Enablement by Clustering Targeting Attributes in CLiEnt <!-- omit in toc -->

- [Product Vision](#product-vision)
- [Privacy Features](#privacy-features)
  - [Block Third-Party Tracking Resources](#block-third-party-tracking-resources)
  - [Third-Party Cookie Blocking](#third-party-cookie-blocking)
  - [Email Aliases](#email-aliases)
  - [Marketing Parameter Removal](#marketing-parameter-removal)
  - [Streamlined Denial of Consent](#streamlined-denial-of-consent)
  - [Proxying Traffic](#proxying-traffic)
- [Advertising Features](#advertising-features)
  - [Local Profile Generation](#local-profile-generation)
    - [Observed Activity](#observed-activity)
    - [Location services](#location-services)
    - [User input](#user-input)
  - [Profile Disclosure](#profile-disclosure)
- [Operations](#operations)
  - [Telemetry](#telemetry)
    - [Lab Study](#lab-study)
    - [Active User Panel](#active-user-panel)
    - [Global Telemetry](#global-telemetry)
  - [Segment Membership Population Estimation](#segment-membership-population-estimation)
- [Other Technologies](#other-technologies)
  - [VPN or Full Proxy](#vpn-or-full-proxy)
  - [Anti-Fingerprinting Technology](#anti-fingerprinting-technology)
- [Risks](#risks)
  - [Fraud](#fraud)
    - [Trusted Computing](#trusted-computing)
    - [User Accounts](#user-accounts)
    - [Passive Systems](#passive-systems)
    - [Active Systems](#active-systems)
    - [Extending Systems](#extending-systems)
  - [Measurement](#measurement)
    - [No User ID](#no-user-id)
    - [Reduced Third-Party Validation](#reduced-third-party-validation)
  - [Sequential Scenarios](#sequential-scenarios)
    - [Attribution](#attribution)
    - [Frequency Capping](#frequency-capping)
    - [Linear/Sequenced Ad Experiences](#linearsequenced-ad-experiences)
  - [Product Viability](#product-viability)

## Product Vision

Is it possible that optional, installable software can attract a sufficient number of users to make a private-by-design ad-targeting system workable? Can a browser extension, custom web browser, or an option within an existing browser or mobile OS garner sufficient interest from users and partners alike to create a meaningful audience for advertisers that want to be ahead of privacy best practices?  

Cookie-based trackers have been the most prominent tool for segmenting and following users and their behavior across the web for years. Now they are nearing their end thanks to anti-tracking policies by Safari and Firefox, and Chrome's announcement to follow in their footsteps, as well as data protection legislations like GDPR, CCPA, and CPRA.

Advertising remains the best method of supporting the free web and allowing content and services to be widely available for all types of users. Without ad funding, access to content and services on the web would be a costly service that only few can afford. However, it is clear that the advertising data segment is drastically changing, with an open call for a solution that reduces the reliance on third-party cookies and increases respect towards users’ privacy.  

Installable software that provides private-by-design ad targeting needs to offer best in-class privacy features at no monetary cost to the user. It also needs to successfully implement said private-by-design ad-targeting system, and overcome a range of problems that are expected in that space. By providing users with all the necessary tools to ensure that they remain unidentified as specific individuals, while informing them transparently about the usage of their information, an enhanced value exchange between content and its audience, as well as service providers and their users, can be established. 

## Privacy Features

By collecting industry-leading privacy features and making them freely available, such a product ensures that all users have a high baseline of privacy. This also becomes a strong market differentiator that drives installations, as users are provided with a toolset that otherwise would have cost them a few USD per month. 

### Block Third-Party Tracking Resources
Simple blocking of HTTP requests to tracking domains based on curated lists or heuristic classification is a baseline feature for privacy products at this time.  

### Third-Party Cookie Blocking 
Blocking third-party cookies is expected to be the norm going forward. However, doing so strictly now can cause noticeable problems in common user scenarios. 

By putting third-party cookies into a sandbox and reducing their duration, most common user scenarios are allowed, while cross-site tracking is prevented. A sandbox keeps third-party cookies strictly associated with the first-party domain that they were set in. For example, a cookie set by thirdpartyA on domain1 would not be sent to thirdpartyA in domain2 context. 


![Cookie sandbox image](./assets/cookie-sandbox.png "Cookie sandbox")


Cookies persist in some situations where a user does not expect, and this behavior can be cleaned up. For example, clearing the sandbox for a first-party domain when all tabs open to that domain are closed, and forcing sandboxed cookies to be session cookies so that they also clear when the browser is closed, ensures that the cookies do not persist longer than the user’s interaction with a specific domain. 

Note that cookies set in CNAME cloaked first-party subdomains would not be able to be automatically sandboxed in Chrome, and improvements to transparency of true origin are necessary. While a fix to this issue can be found in some browsers, (Safari, for example), this browser exposure requires a broader solution. 


### Email Aliases
Use of the same email address across multiple websites provides the foundation for cross-site tracking and targeting when consistent third-party cookies are no longer available. 

While some companies offer aliases for privacy (like [Firefox Relay](https://relay.firefox.com/)), these are not currently promoted specifically to dissuade cross-site tracking. Instead, they focus on the security aspect. 

A better implementation is to offer sufficient aliases so that the user can use a different email address on each different domain that requests registration—to keep their primary email secret from registration systems, to keep their primary email away from hackers, and to turn off spam.  

![Cross site alias image](./assets/cross-site-alias.png "Cross site alias")


The alias@relay.provider convention established by [Mozilla](https://relay.firefox.com/faq) and [Apple](https://developer.apple.com/documentation/sign_in_with_apple/sign_in_with_apple_js/communicating_using_the_private_email_relay_service) is interpreted as a clear statement that the registrant does not want to be tracked across apps and sites, and registration systems would treat it as such. 

### Marketing Parameter Removal

As browsers and extensions [phase out](https://blog.chromium.org/2020/01/building-more-private-web-path-towards.html) [support](https://webkit.org/blog/9521/intelligent-tracking-prevention-2-3/) for third-party cookies, advertising systems are finding innovative ways to bring tracking identities from first-party domains. 

A solution that aims to offer industry-leading privacy needs to keep up-to-date with these innovations and block them. Information can be transferred using HTTP REFERER, link decoration, and other mechanisms. 

![HTTP request modification image](./assets/http-request-modification.png "HTTP request modification")

### Streamlined Denial of Consent

In applicable markets across Europe, for example, websites and their Consent Management Platforms (CMPs) may choose a user experience that makes it difficult to find the option to deny consent to tracking cookies. 

By seeking a double opt-in to deny all requests for consent, a client-side product can have a clear user mandate to streamline the experience and ensure that the user does not accidentally grant consent due to a confusing or aggressive set of options presented by the website. 

Offering this type of feature allows a product to clearly identify that only its own provided profile is available for targeting. 

### Proxying Traffic
Advertising systems can make household-level targeting decisions based on the IP address of the connecting client. Proxying traffic prevents origin IP from being available to the ad system. However, this has implications in terms of operational costs and performance. 

Sending only bidding and advertising traffic through a privacy proxy, while allowing non-advertising traffic to flow directly to the servers, can optimize for privacy, performance, and reduced operational costs. 

![Proxying traffic image](./assets/proxying-traffic.png "Proxying traffic")

Concerns of doing server-side data collection might be raised, but the client-side code has access to more information in such a configuration. Maintaining a list of advertising systems is already done by [multiple](https://disconnect.me/trackerprotection) [entities](https://easylist.to/). 

## Advertising Features

With this strong baseline of privacy, a client-side targeting system has an exclusive audience to make available to buyers. To support the ad-funded web and ensure that content creators continue to have means to generate revenue, a client-side ad-targeting solution needs to ensure that the sites visited are given a share of the value generated from the information used to deliver useful advertising. 

### Local Profile Generation
Current advertising profiles are based on observed activity, registration data, and offline match. Finding equivalents for a fully client-side profile can be done as follows. 

#### Observed Activity
Websites that a user visits and interacts with are commonly used to determine interest categories. Two meaningfully different implementations are possible for the software:

- The category of the website can be assigned by a server-side process, and a list of classifications based on portions of the URL sent to the software.  
- The category of the website can be discovered by the software interpreting the content on the website.  

Either implementation allows a complete overview of user activity, providing better user insight than available with the patchwork of third-party data collection systems.  

#### Location services
Requesting location insight from the device or the user allows on-client generalization to larger geographic areas. Since privacy regulations and practices differ between countries, and sometimes within countries (e.g. US), it would be most useful if the technology declares 'country' in most cases, and 'country sub-region' where necessary. 

By determining a reasonable geographic granularity on the client, the user is provided with meaningful transparency and control that is not available with server solutions. 

#### User input
Inferring demographic data—like age and gender—is far from perfect, and a fully client-side profile would not be able to leverage the offline match or other registration systems. Asking the user for demographic information keeps them in control over what is known about them and avoids incorrect inferences.  

### Profile Disclosure
An advertising profile is only of value if it can be used as input into ad decisioning. A system that offers strong privacy features, as described, integrates with existing bidding and ad-serving systems with minimal changes, ultimately bypassing the significant change to operations in proposals like [TURTLEDOVE](https://github.com/WICG/turtledove), [Dovekey](https://github.com/google/ads-privacy/tree/master/proposals/dovekey), [Sparrow](https://github.com/WICG/sparrow), and [PARRROT](https://github.com/prebid/identity-gatekeeper/blob/master/proposals/PARRROT.md). 

A system that sends a variable subset of active segment memberships based on a reasonable algorithm offers meaningful information for decisioning on the ad server to offset the impact of losing advertising identity, while also minimizing the risk that advertising systems could fingerprint based on segment membership.

![Selective profile disclosure image](./assets/selective-profile-disclosure.png "Selective profile disclosure")

An implementation might only share some of the strongest interests at any given time. For example, it might tell an advertising system that a user is interested in sports, cars, and gyms one time, and gaming consoles, cars, and electronics the next. 

Additionally, the full suite of privacy features continues to protect user privacy. 

## Operations

### Telemetry
Understanding how a product is used is important to improving that product. A few options exist to gathering usage information:

#### Lab Study
By separating normal user behavior from testing scenarios, the user’s privacy is strongly protected. While lab testing provides tremendous insight, it does have significant downsides, including cost, small scope, and abnormal user behavior. 

#### Active User Panel
By seeking consent from users to report anonymized product usage information, a panel is established. The product further protects shared information by implementing k-anonymity (suppression—disclosing less than what was recorded) or differential privacy (lying—disclosing something slightly different than what was recorded), or by simply ensuring that there are few enough data points compared to panel population to be in a k-anonymous state to begin with. 

#### Global Telemetry
By requiring the collecting of anonymized product usage information in order to use the product, the largest possible pool of users would potentially be established, although some installs might be lost compared to the opt-in panel method. This allows the largest set of data points to be collected while still ensuring a k-anonymous state. A product in an explicitly “beta” state would be best positioned to hold this position. 

### Segment Membership Population Estimation
In order to ensure that no segment containing a very small number of users is made available for targeting, the buyer-facing systems need to understand the rough population of each interest category.

This information is collected using differential privacy techniques. Each client reports their total list of membership to a server dedicated to estimating populations. However, clients sometimes report incorrect membership to the server, indicating membership to segments that they are not members of, and non-membership to segments in which they are actual members. 

Additionally, sensible minimum segment populations need to be enforced in the sales process. 

## Other Technologies
This proposal skips some seemingly obvious privacy functionality. 

### VPN or Full Proxy
Both of these techniques minimize the possibility of a publisher sharing the actual IP address of the client with the advertising system. The increased operational cost, increased dependency on the product provider’s infrastructure for normal web browsing, and availability of other techniques to strip IPs (and other identifiers) from being shared as link decoration make the selective proxy a more sensible implementation. 

A VPN aims to pass all network traffic through a new IP address without otherwise modifying it. This has more operational overhead and might exceed the scope of a product aiming at delivering better, more private ad experiences. 

A full proxy sends all web traffic through the product owner’s servers. This allows the product provider to observe all web activity—a privacy risk for the user. 

### Anti-Fingerprinting Technology
Browsers are getting [better at](https://www.apple.com/safari/docs/Safari_White_Paper_Nov_2019.pdf) [anti-fingerprinting](https://blog.mozilla.org/security/2020/01/07/firefox-72-fingerprinting/). Anything that a privacy product can do in anti-fingerprinting will tend to differentiate those users with the tool from those who do not have the tool, providing a way of detecting product users and thereby treating them differently. 

## Risks
There are risks intrinsic to client-side ad-targeting profiles. 

### Fraud
When segment membership is created within the client, and the client is hardened against oversight by third parties, it is easy for a client to lie to the advertising systems about actual segment membership. 

While traditional ad fraud usually needs a period of innocuous activity in order to build up a useful advertising profile, an attack on a client-side ad-profile system could simply report valuable segment memberships to ad systems integrated into their payout site. 

There are potentially methods to mitigate fraud, and a Swiss cheese model, encompassing a variety of imperfect options, will likely be the path forward. Some methods include:

#### Trusted Computing
Ensuring that only verified code executes by using on-device encrypted client-side code by using security chips and rootkits. Since this forces the user to run code that they cannot oversee, the premise of transparency and control is broken.   

#### User Accounts
Offloading trust of the client to sign in systems (Google, Apple, Facebook, etc.) allows a separate registration system to manage identity and increase the cost of performing fraud.  However, required registration forces the user to become known, breaking the fundamental premise of anonymity.  

#### Passive Systems
Observing advertising activity at the proxy server allows the detection of anomalous behavior on the basis of actual IP addresses. Doing anti-fraud processes on the proxy server introduces additional operational overhead and necessitates preserving some data for pattern recognition, breaking some expectations of privacy that the user might have.  

Leveraging product telemetry to understand actual feature usage allows the classification of users into types. These types can be associated with expected behavior parameters, making anomalous behavior detection faster. However, bridging telemetry insights to specific instances of the product requires definitive identification of the client, breaking the premise of anonymity.  

#### Active Systems
Inducing the installed product to execute special behaviors, like accept membership in a non-relevant segment or make a request to a specific URLs, allow a basic check of client integrity. This makes it more complicated and expensive for fraudsters to replicate the system.  However, reliable integrity checks are expensive to maintain and make user verification of the product more difficult.  

Interrupting the user to perform CAPTCHAs ensures that a human is engaged with the product. However, the interruptions might negatively impact the user experience.  

#### Extending Systems
Determining when an instance of the product is trustworthy is expensive. Implementing technology like PrivacyPass extends the lifetime of that trustworthy state. However, the downside of incorrectly trusting a client is magnified with this technology.  

### Measurement 
Digital advertising expects that certain certified parties have access to measure audience behavior across a variety of scenarios. This access is changing, and many measurement scenarios are impacted.  

#### No User ID
Fundamental metrics like impressions and clicks are no longer associated with a specific client ID. They may be able to be associated with a cohort ID (identifying a set of segment memberships). This changes the data available to systems that aim to tune campaigns during execution, as well as other measurement systems.  

#### Reduced Third-Party Validation
The product minimizes access by third-party systems. This impacts the ability of measurement systems to validate the metrics reported by the systems allowed to deliver ads. 

### Sequential Scenarios
Having a consistent, unique identity associated with a client enables some fundamental sequential advertising scenarios. This includes delivering a series of ads that tell a coherent story, and capping the number of times a specific ID is permitted to see the same ad. 

Asking the product to maintain a certain amount of awareness about what ads have been delivered allows some of these scenarios to return, but requires modification or complete reworking to the infrastructure of trust. 

#### Attribution
Being able to understand what exposures led to a specific outcome is the goal of attribution. Without a consistent identification, it is not possible to gather this information anywhere except for on the client. 

However, trusting the client to do attribution provides a great opportunity for manipulation by bad actors. 

Attribution models may be updated to operate at a more coarse level of granularity, like cohorts. 

#### Frequency Capping
Setting a maximum number of times an ad can be delivered to a specific user is a key feature that minimizes unnecessary spend while also improving user experience. This is controlled by user ID. 

Providing clear ad IDs to the client allows this to be controlled on the client side, but introduces additional requirements of communicating when an ad is not allowed to render due to frequency caps, and a process to ensure a replacement is available to ensure continued publisher revenue. 

#### Linear/Sequenced Ad Experiences
Some campaigns deliver a series of ads in a specific order. This is done to tell a story, or to induce a user to return to a shopping cart. Introducing additional ad management logic in the client allows these scenarios.  

### Product Viability
The product needs to have a sufficiently large install base to be attractive to buyers. The additional overhead of proxy and email relay services, as well as other features, means that generating advertiser demand for the generated targeting profiles is similarly necessary for the long-term viability of the product. 




